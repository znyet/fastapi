﻿using FastEmit;
using System;
using System.Collections.Generic;

namespace FastApi
{
    public class FastModule
    {
        public FastModule(Type classType, ConstructorInvoker invoker, string routeName, bool clearFilter, bool logTime)
        {
            Type = classType;
            Invoker = invoker;
            RouteName = routeName;
            ClearFilter = clearFilter;
            LogTime = logTime;
        }

        /// <summary>
        /// 类类型
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// 构造函数委托
        /// </summary>
        public ConstructorInvoker Invoker { get; }

        /// <summary>
        /// 构造函数参数类型列表
        /// </summary>
        public List<FastParam> ParamList { get; } = new();

        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; }

        /// <summary>
        /// 是否清除过滤器
        /// </summary>
        public bool ClearFilter { get; }

        /// <summary>
        /// 方法字典
        /// </summary>
        internal readonly Dictionary<string, FastAction> ActionDict = new();

        /// <summary>
        /// 特性列表
        /// </summary>
        public List<object> AttributeList { get; } = new();

        /// <summary>
        /// 记录运行时间
        /// </summary>
        public bool LogTime { get; }

        /// <summary>
        /// 是否必须释放
        /// </summary>
        internal bool NeedDispose { get; set; }

        /// <summary>
        /// 是否是事务
        /// </summary>
        internal bool IsTran { get; set; }
    }
}
