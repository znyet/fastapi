﻿using System;
using System.Threading.Tasks;

namespace FastApi
{
    /// <summary>
    /// 参数制造器
    /// </summary>
    public class FastParamCreator
    {
        public Func<FastContext, object> Create;

        public Func<FastContext, Task<object>> CreateAsync;

        public Action<FastContext, object> Dispose;

        public Func<FastContext, object, Task> DisposeAsync;

        public Action<FastContext, object> Commit;

        public Func<FastContext, object, Task> CommitAsync;

        public Action<FastContext, object> Rollback;

        public Func<FastContext, object, Task> RollbackAsync;
    }
}
