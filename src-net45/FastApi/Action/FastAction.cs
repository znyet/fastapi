﻿using FastEmit;
using System;
using System.Collections.Generic;

namespace FastApi
{
    public class FastAction
    {
        public FastAction(string httpMethod, string routeName, string methodName, bool clearFilter,
            MethodInvoker invoker, bool isStatic, Type returnType, FastActionType actionType, FastActionTypeTask typeTask, bool logTime)
        {
            HttpMethod = httpMethod.ToUpper();
            RouteName = routeName;
            MethodName = methodName;
            ClearFilter = clearFilter;
            Invoker = invoker;
            IsStatic = isStatic;
            ActionType = actionType;
            ActionTypeTask = typeTask;
            ReturnType = returnType;
            LogTime = logTime;
        }

        public string HttpMethod { get; }

        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; }

        /// <summary>
        /// 方法名称
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// 是否清除过滤器
        /// </summary>
        public bool ClearFilter { get; }

        /// <summary>
        /// 方法委托
        /// </summary>
        public MethodInvoker Invoker { get; }

        /// <summary>
        /// 方法参数类型列表
        /// </summary>
        public List<FastParam> ParamList { get; } = new();

        /// <summary>
        /// 是否静态方法
        /// </summary>
        public bool IsStatic { get; }

        /// <summary>
        /// 方法返回类型
        /// </summary>
        public Type ReturnType { get; }

        /// <summary>
        /// 方法类型
        /// </summary>
        public FastActionType ActionType { get; }

        /// <summary>
        /// 方法返回异步类型判断
        /// </summary>
        public FastActionTypeTask ActionTypeTask { get; }

        /// <summary>
        /// 特性列表
        /// </summary>
        public List<object> AttributeList { get; } = new();

        /// <summary>
        /// 记录运行时间
        /// </summary>
        public bool LogTime { get; }

        /// <summary>
        /// 是否必须释放
        /// </summary>
        internal bool NeedDispose { get; set; }

        /// <summary>
        /// 是否是事务
        /// </summary>
        internal bool IsTran { get; set; }


    }
}
