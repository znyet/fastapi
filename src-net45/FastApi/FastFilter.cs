﻿using System;
using System.Threading.Tasks;

namespace FastApi
{
    public class FastFilter
    {
        public Func<FastContext, bool> Execute;

        public Func<FastContext, Task<bool>> ExecuteAsync;
    }
}
