﻿using Microsoft.Owin;
using System.Collections.Generic;

namespace FastApi
{
    public class FastContext
    {
        public FastContext(IOwinContext httpContext, FastModule module, FastAction action)
        {
            HttpContext = httpContext;
            Module = module;
            Action = action;
        }

        /// <summary>
        /// http请求上下文
        /// </summary>
        public IOwinContext HttpContext { get; }

        /// <summary>
        /// 类模块
        /// </summary>
        public FastModule Module { get; }

        /// <summary>
        /// 方法
        /// </summary>
        public FastAction Action { get; }

        /// <summary>
        /// 用户自定义数据
        /// </summary>
        public Dictionary<string, object> UserData { get; } = new();

        /// <summary>
        /// 请求数据
        /// </summary>
        public object[] RequestData { get; set; }

        /// <summary>
        /// 响应原始数据
        /// </summary>
        public object ResponseData { get; set; }

        /// <summary>
        /// 方法执行时间（毫秒）
        /// </summary>
        public long ExecuteTime { get; set; }

        /// <summary>
        /// json序列化时间
        /// </summary>
        public long JsonSerialTime { get; set; }

        /// <summary>
        /// from表单
        /// </summary>
        public IFormCollection Form { get; set; }
    }
}
