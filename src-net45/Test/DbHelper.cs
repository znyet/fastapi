﻿using Dapper.Sharding;

namespace Test
{
    public class DbHelper
    {
        public static readonly IClient Client;

        public static readonly IDatabase Database;

        public static readonly ITable<People> peopleTable;

        static DbHelper()
        {
            var config = new DataBaseConfig { Password = "123456" };
            Client = ShardingFactory.CreateClient(DataBaseType.MySql, config);
            Client.AutoCompareTableColumn = true;
            Database = Client.GetDatabase("test");
            peopleTable = Database.GetTable<People>("people");
        }
    }



}
