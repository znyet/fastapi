﻿using Microsoft.Owin.Hosting;
using System;

namespace Test
{
    internal class Program
    {
        static void Main(string[] args)
        {
            WebApp.Start<Startup>("http://*");
            Console.WriteLine("启动成功");
            Console.ReadKey();
        }
    }
}
