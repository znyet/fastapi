﻿using Dapper.Sharding;

namespace Test
{
    [Table("Id", true)]
    public class People
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
