﻿using FastApi.Attributes;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Dapper.Sharding;

namespace Test.Module
{
    [FastRoute("test")]
    public class TestModule
    {
        public string Index()
        {
            return "test index中文";
        }

        public Task<string> AddAsync()
        {
            return Task.FromResult("add async");
        }

        [FastCustomer]
        public async Task FileAsync(IFastHttp http)
        {
            var keys = http.GetQueryKeys();
            var bytes = new byte[] { 1, 2, 3, 4 };
            await http.WriteFileAsync(bytes, "文件abc123.zip");
        }

        public People Query(People p)
        {
            return p;
        }

        public People Form([FastForm] People p)
        {
            return p;
        }

        [FastPost]
        public People Body(People p, JObject obj)
        {
            return p;
        }

        public string AA([FastBody] string a)
        {
            return a;
        }

        [FastRedirect]
        public string Baidu()
        {
            return "https://www.baidu.com";
        }

        public People db(DistributedTransaction tran)
        {
            var p = new People { Name = "李四" };
            DbHelper.peopleTable.Insert(p, tran);
            return p;
        }

        public People db2(DistributedTransaction tran)
        {
            DbHelper.peopleTable.Insert(new People { Name = "李四" }, tran);
            throw new System.Exception("事务错误");
        }
    }
}
