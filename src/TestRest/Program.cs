﻿using OkFlurl;
using System.Text.Json.Nodes;

namespace TestRest
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var client = new RestHelper("http://localhost:5212");

            await client.Home_A<string>(new { Name = "里斯", Id = 100 });

            return;

            var a = await client.Home_Add<string>(new { a = "Lili" });
            Console.WriteLine(a);

            var client2 = RestHelper.GetCache("http://localhost:5212");
            var a2 = await client2.Home_Add<string>(new { a = "Lili222" });
            Console.WriteLine(a2);

            var a3 = await client.Test2_Getdddd<string>(new { id = 1, name = "里斯", money = 100.2, time = DateTime.Now });
            Console.WriteLine(a3);

            a3 = await client.Test2_GetUpload2<string>(new { name = "哈哈哈", time = DateTime.Now }, null);
            Console.WriteLine(a3);

            var form1 = new FlurlForm();
            form1.AddFile("file", @"C:\Users\Administrator\Desktop\什么是Blazor.docx");
            var ks = await client.Test2_GetUpload3(new { name = "1" }, form1);

            var form2 = new FlurlForm();
            form2.AddFile("file", @"C:\Users\Administrator\Desktop\什么是Blazor.docx");
            var kd = await client.FormDownloadBytesAsync("/test2/getUpload3", new { name = "1" }, form2);

            var v = kd.bytes;
            var name = kd.name;

            Console.WriteLine(name);

            await client.Test_ZZ<JsonNode>();

            Console.WriteLine("Hello, World!");
            Console.ReadKey();
        }
    }
}
