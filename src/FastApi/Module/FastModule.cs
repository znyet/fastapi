﻿using FastEmit;

namespace FastApi
{
    public class FastModule
    {
        public FastModule(Type classType, ConstructorInvoker invoker, string routeName, bool clearFilter)
        {
            Type = classType;
            Invoker = invoker;
            RouteName = routeName;
            ClearFilter = clearFilter;
        }

        /// <summary>
        /// 类类型
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// 构造函数委托
        /// </summary>
        public ConstructorInvoker Invoker { get; }

        /// <summary>
        /// 构造函数参数类型列表
        /// </summary>
        public List<FastParam> ParamList { get; } = [];

        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; }

        /// <summary>
        /// 是否清除过滤器
        /// </summary>
        public bool ClearFilter { get; }

        /// <summary>
        /// 方法字典
        /// </summary>
        public readonly Dictionary<string, FastAction> ActionDict = [];

        /// <summary>
        /// 特性列表
        /// </summary>
        public List<object> AttributeList { get; } = [];

        /// <summary>
        /// 是否必须释放
        /// </summary>
        internal bool NeedDispose { get; set; }

        /// <summary>
        /// 是否是事务
        /// </summary>
        internal bool IsTran { get; set; }

        /// <summary>
        /// 限流个数
        /// </summary>
        internal int _limitCount;
        public int RateLimitCount => _limitCount;

        /// <summary>
        /// 限流key
        /// </summary>
        internal string _limitKey;
        public string RateLimitKey => _limitKey;

        /// <summary>
        /// 限流错误信息
        /// </summary>
        internal string _limitMsg;
        public string RateLimitMsg => _limitMsg;

        /// <summary>
        /// 限流错误码
        /// </summary>
        internal int _limitCode;
        public int RateLimitCode => _limitCode;

    }
}
