﻿using FastApi.Attributes;
using System.Text.Json;

namespace FastApi
{
    public class FastContext
    {
        public FastContext(HttpContext httpContext, FastModule module, FastAction action, JsonSerializerOptions options, string routeData)
        {
            HttpContext = httpContext;
            Module = module;
            Action = action;
            _option = options;
            ClearFilter = Module.ClearFilter || Action.ClearFilter;
            RouteData = routeData;
        }

        internal JsonSerializerOptions _option;
        public JsonSerializerOptions JsonOptions => _option;

        /// <summary>
        /// http请求上下文
        /// </summary>
        public HttpContext HttpContext { get; }

        /// <summary>
        /// 令牌
        /// </summary>
        public CancellationToken Token => HttpContext.RequestAborted;

        /// <summary>
        /// Form表单
        /// </summary>
        internal IFormCollection _form;
        public IFormCollection Form => _form;

        /// <summary>
        /// 类模块
        /// </summary>
        public FastModule Module { get; }

        /// <summary>
        /// 方法
        /// </summary>
        public FastAction Action { get; }

        /// <summary>
        /// 路由
        /// </summary>
        public string Route => Action.Path;

        /// <summary>
        /// 是否清除过滤器
        /// </summary>
        public bool ClearFilter { get; }

        /// <summary>
        /// 用户自定义数据
        /// </summary>
        public Dictionary<string, object> UserData { get; } = [];

        /// <summary>
        /// 方法参数
        /// </summary>
        public object[] MethodParams { get; set; }

        /// <summary>
        /// 响应原始数据
        /// </summary>
        public object ResponseData { get; set; }

        /// <summary>
        /// 方法执行时间（毫秒）
        /// </summary>
        internal double _execTime;
        public double ExecuteTime => _execTime;

        /// <summary>
        /// json序列化时间
        /// </summary>
        internal double _seriTime;
        public double SerialTime => _seriTime;

        /// <summary>
        /// FastHttp
        /// </summary>
        internal IFastHttp _fastHttp;
        public IFastHttp FastHttp
        {
            get
            {
                _fastHttp ??= new FastHttp(this);
                return _fastHttp;
            }
        }

        /// <summary>
        /// 全局异常
        /// </summary>
        public Exception Ex { get; set; }

        /// <summary>
        /// 路由数据
        /// </summary>
        public string RouteData { get; }

        /// <summary>
        /// 客户自定义
        /// </summary>
        internal bool _customer;
        internal bool Customer => _customer;

        /// <summary>
        /// 是否可以响应
        /// </summary>
        internal bool _canResponse;
        public bool CanResponse => _canResponse;

        /// <summary>
        /// 获取请求数据
        /// </summary>
        /// <returns></returns>
        public List<object> GetRequestData()
        {
            if (MethodParams == null || MethodParams.Length == 0)
            {
                return [];
            }
            List<object> list = [];
            for (int i = 0; i < MethodParams.Length; i++)
            {
                var par = Action.ParamList[i];
                if (!par.IsCustomerType)
                {
                    list.Add(MethodParams[i]);
                }
            }
            return list;
        }
    }
}
