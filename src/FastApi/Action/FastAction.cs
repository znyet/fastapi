﻿using FastEmit;

namespace FastApi
{
    public class FastAction
    {
        public FastAction(string httpMethod, string routeName, string methodName, bool clearFilter,
            MethodInvoker invoker, bool isStatic, Type returnType, FastActionType actionType, FastActionTypeTask typeTask, 
            bool enableBuffer, string path)
        {
            HttpMethod = httpMethod.ToUpper();
            RouteName = routeName;
            Path = path;
            MethodName = methodName;
            ClearFilter = clearFilter;
            Invoker = invoker;
            IsStatic = isStatic;
            ActionType = actionType;
            ActionTypeTask = typeTask;
            ReturnType = returnType;
            EnableBuffering = enableBuffer;
        }

        public string HttpMethod { get; }

        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; }

        /// <summary>
        /// 完整路径
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// 方法名称
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        /// 是否清除过滤器
        /// </summary>
        public bool ClearFilter { get; }

        /// <summary>
        /// 方法委托
        /// </summary>
        public MethodInvoker Invoker { get; }

        /// <summary>
        /// 方法参数类型列表
        /// </summary>
        public List<FastParam> ParamList { get; } = [];

        /// <summary>
        /// 是否静态方法
        /// </summary>
        public bool IsStatic { get; }

        /// <summary>
        /// 方法返回类型
        /// </summary>
        public Type ReturnType { get; }

        /// <summary>
        /// 方法类型
        /// </summary>
        public FastActionType ActionType { get; }

        /// <summary>
        /// 方法返回异步类型判断
        /// </summary>
        public FastActionTypeTask ActionTypeTask { get; }

        /// <summary>
        /// 特性列表
        /// </summary>
        public List<object> AttributeList { get; } = [];

        /// <summary>
        /// 是否必须释放
        /// </summary>
        internal bool NeedDispose { get; set; }

        /// <summary>
        /// 是否是事务
        /// </summary>
        internal bool IsTran { get; set; }

        /// <summary>
        /// 是否允许重复读取body
        /// </summary>
        internal bool EnableBuffering { get; }

        /// <summary>
        /// 限流个数
        /// </summary>
        internal int _limitCount;
        public int RateLimitCount => _limitCount;

        /// <summary>
        /// 限流key
        /// </summary>
        internal string _limitKey;
        public string RateLimitKey => _limitKey;

        /// <summary>
        /// 限流错误信息
        /// </summary>
        internal string _limitMsg;
        public string RateLimitMsg => _limitMsg;

        /// <summary>
        /// 限流错误码
        /// </summary>
        internal int _limitCode;
        public int RateLimitCode => _limitCode;

        /// <summary>
        /// 是否是Json异步流
        /// </summary>
        internal bool _isStream;
        public bool IsJsonStream => _isStream;

        /// <summary>
        /// 是否传入流内容
        /// </summary>
        internal bool _isPosts;
        public bool IsPostStream => _isPosts;

        /// <summary>
        /// 是否是表单
        /// </summary>
        internal bool _isForm;
        public bool IsForm => _isForm;

        /// <summary>
        /// 是否上传文件
        /// </summary>
        internal bool _isUpload;
        public bool IsUpload => _isUpload;

        /// <summary>
        /// 是都下载
        /// </summary>
        internal bool _isDown;
        public bool IsDownload => _isDown;
    }
}
