﻿namespace FastApi
{
    public enum FastActionType
    {
        /// <summary>
        /// 默认返回
        /// </summary>
        Default = 0,

        /// <summary>
        /// 自定义
        /// </summary>
        Customer = 1,

        /// <summary>
        ///  跳转
        /// </summary>
        Redirect = 2,

        /// <summary>
        /// 视图
        /// </summary>
        View = 4
    }
}
