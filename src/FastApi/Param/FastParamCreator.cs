﻿namespace FastApi
{
    /// <summary>
    /// 参数制造器
    /// </summary>
    public class FastParamCreator
    {
        public Func<FastContext, object> Create;

        public Func<FastContext, Task<object>> CreateAsync;

        public Func<FastContext, ValueTask<object>> CreateValueAsync;

        public Action<FastContext, object> Dispose;

        public Func<FastContext, object, Task> DisposeAsync;

        public Func<FastContext, object, ValueTask> DisposeValueAsync;

        public Action<FastContext, object> Commit;

        public Func<FastContext, object, Task> CommitAsync;

        public Func<FastContext, object, ValueTask> CommitValueAsync;

        public Action<FastContext, object> Rollback;

        public Func<FastContext, object, Task> RollbackAsync;

        public Func<FastContext, object, ValueTask> RollbackValueAsync;
    }
}
