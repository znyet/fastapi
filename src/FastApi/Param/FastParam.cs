﻿using FastApi.Attributes;

namespace FastApi
{
    public class FastParam
    {
        public FastParam(Type type, string name, bool simple, FastParamType reqType, FastParamCreator creator)
        {
            Type = type;
            Name = name;
            IsSimpleType = simple;
            RequestType = reqType;
            Creator = creator;

            if (creator != null)
            {
                IsCustomerType = true;
                if (creator.Dispose != null || creator.DisposeAsync != null || creator.DisposeValueAsync != null)
                {
                    NeedDispose = true;
                }

                if (creator.Commit != null || creator.CommitAsync != null || creator.CommitValueAsync != null)
                {
                    IsTran = true;
                }
            }
            else if (type == typeof(HttpContext) || type == typeof(HttpRequest)
                || type == typeof(HttpResponse) || type == typeof(IFastHttp))
            {
                IsCustomerType = true;
            }
        }

        /// <summary>
        /// 参数类型
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// 是不是简单类型
        /// </summary>
        public bool IsSimpleType { get; }

        /// <summary>
        /// 是不是自定义类型
        /// </summary>
        public bool IsCustomerType { get; }

        /// <summary>
        /// 参数名称
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 参数接收http请求值类型
        /// </summary>
        public FastParamType RequestType { get; }

        /// <summary>
        /// 特性列表
        /// </summary>
        public List<object> AttributeList { get; } = [];

        /// <summary>
        /// 参数制造器
        /// </summary>
        public FastParamCreator Creator { get; }

        /// <summary>
        /// 是否必须释放
        /// </summary>
        public bool NeedDispose { get; }

        /// <summary>
        /// 是否是事务
        /// </summary>
        public bool IsTran { get; }
    }
}
