﻿namespace FastApi
{
    public enum FastParamType
    {
        Query = 0,
        Form = 1,
        Body = 2
    }
}
