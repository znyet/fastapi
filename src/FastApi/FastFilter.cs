﻿namespace FastApi
{
    public class FastFilter
    {
        public Action<FastContext> Execute;

        public Func<FastContext, Task> ExecuteAsync;

        public Func<FastContext, ValueTask> ExecuteValueAsync;
    }
}
