﻿namespace Test.Dto
{
    public class BaseEntity<T>
    {
        public T Data { get; set; }

        public List<T> DataList { get; set; }
    }
}
