﻿using NSwag;

namespace FastApi.Swag
{
    internal static class FastSwagUtils
    {
        public static void AddApiKey(this OpenApiDocument doc, string key, OpenApiSecurityApiKeyLocation location = OpenApiSecurityApiKeyLocation.Header)
        {
            var keyName = key;
            var securityScheme = new OpenApiSecurityScheme
            {
                Name = keyName,
                In = location,
                Type = OpenApiSecuritySchemeType.ApiKey
            };

            doc.SecurityDefinitions.Add(keyName, securityScheme);
            var sec = new OpenApiSecurityRequirement
            {
                { keyName, Enumerable.Empty<string>() }
            };
            doc.Security.Add(sec);
        }

        public static void AddJwtBearer(this OpenApiDocument doc)
        {
            var keyName = "Authorization Bearer";
            var securityScheme = new OpenApiSecurityScheme
            {
                Name = keyName,
                In = OpenApiSecurityApiKeyLocation.Header,
                Type = OpenApiSecuritySchemeType.Http,
                Scheme = "Bearer",
                BearerFormat = "JWT"
            };

            doc.SecurityDefinitions.Add(keyName, securityScheme);
            var sec = new OpenApiSecurityRequirement
            {
                { keyName, Enumerable.Empty<string>() }
            };
            doc.Security.Add(sec);
        }


    }
}
