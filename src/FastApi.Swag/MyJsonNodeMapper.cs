﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NJsonSchema;
using NJsonSchema.Generation.TypeMappers;
using System.Text.Json.Nodes;

namespace FastApi.Swag
{
    internal class MyJsonNodeMapper : ITypeMapper
    {
        public Type MappedType => typeof(JsonNode);

        public bool UseReference => false;

        public void GenerateSchema(JsonSchema schema, TypeMapperContext context)
        {
            schema.Example = JsonConvert.DeserializeObject<JToken>("");
        }
    }
}
