﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NJsonSchema;
using NJsonSchema.Generation.TypeMappers;
using System.Text.Json.Nodes;

namespace FastApi.Swag
{
    internal class MyJsonObjectMapper : ITypeMapper
    {
        public Type MappedType => typeof(JsonObject);

        public bool UseReference => false;

        public void GenerateSchema(JsonSchema schema, TypeMapperContext context)
        {
            schema.Type = JsonObjectType.Object;
            schema.Example = JsonConvert.DeserializeObject<JObject>("{}");
        }
    }
}
