﻿using NJsonSchema.Generation;

namespace FastApi.Swag
{
    internal class MySchemaName : ISchemaNameGenerator
    {
        public string Generate(Type type)
        {
            return type.FullName.Replace(".", "_");
        }
    }
}
