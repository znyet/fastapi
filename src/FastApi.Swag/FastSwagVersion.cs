﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastApi.Swag
{
    public enum FastSwagVersion
    {
        V40 = 0,
        V50 = 1
    }
}
