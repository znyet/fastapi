﻿using NJsonSchema;
using NSwag;

namespace FastApi.Swag
{
    internal class FastSwagPar
    {
        public OpenApiParameterKind Kind { get; set; }

        public string Name { get; set; }

        public JsonObjectType ObjectType { get; set; } = JsonObjectType.String;

        public bool IsRequired { get; set; }

        public string Description { get; set; }

        public Type CsType { get; set; } = typeof(string);
    }
}
