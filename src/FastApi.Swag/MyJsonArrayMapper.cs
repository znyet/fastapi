﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NJsonSchema;
using NJsonSchema.Generation.TypeMappers;
using System.Text.Json.Nodes;

namespace FastApi.Swag
{
    internal class MyJsonArrayMapper : ITypeMapper
    {
        public Type MappedType => typeof(JsonArray);

        public bool UseReference => false;

        public void GenerateSchema(JsonSchema schema, TypeMapperContext context)
        {
            schema.Type = JsonObjectType.Array;
            schema.Example = JsonConvert.DeserializeObject<JArray>("[]");
        }
    }
}
