﻿using Autofac;

namespace Test
{
    public class AutofacHelper
    {
        private readonly static IContainer container;
        static AutofacHelper()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MySqlDatabase>().AsImplementedInterfaces();
            builder.RegisterType<NpgsqlDatabase>().Named<IMyDatabase>("pg");
            container = builder.Build();
        }

        public static ILifetimeScope GetScope()
        {
            return container.BeginLifetimeScope();
        }
    }

    public interface IMyDatabase
    {
        string Get();
    }

    public class MySqlDatabase : IMyDatabase
    {
        public string Get()
        {
            return "MySql Database";
        }
    }

    public class NpgsqlDatabase : IMyDatabase
    {
        public string Get()
        {
            return "Npgsql Database";
        }
    }
}
