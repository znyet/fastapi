﻿using Dapper.Sharding;

namespace Test
{
    [Table("Id")]
    public class People
    {
        /// <summary>
        /// 自增id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? AddTime { get; set; }

        /// <summary>
        /// 添加时间2
        /// </summary>
        public DateTime? AddTime2 { get; set; }

        /// <summary>
        /// 编辑时间
        /// </summary>
        public DateTime? EditTime { get; set; }

        /// <summary>
        /// 编辑时间2
        /// </summary>
        public DateTime? EditTime2 { get; set; }
        
        /// <summary>
        /// 计算1
        /// </summary>
        public int COUNTNUM { get; set; }

        /// <summary>
        /// 计算2
        /// </summary>
        public int COUnTNUM2 { get; set; }
    }
}
