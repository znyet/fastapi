﻿using Dapper.Sharding;

namespace Test
{
    public class DbHelper
    {
        public static readonly IClient Client;

        public static readonly IDatabase Database;

        public static  ITable<People> peopleTable=> Database.GetTable<People>("people");

        static DbHelper()
        {
            var config = new DataBaseConfig { Password = "123456" };
            Client = ShardingFactory.CreateClient(DataBaseType.Postgresql, config);
            Client.AutoCreateDatabase = false;
            Client.AutoCreateTable = false;
            Client.AutoCompareTableColumn = false;
            Database = Client.GetDatabase("test");
        }
    }



}
