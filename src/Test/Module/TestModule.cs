﻿using FastApi.Attributes;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using Test.Dto;

namespace Test
{
    [FastFilterClear]
    public class TestModule
    {
        public string Index(IFastHttp _http)
        {
            var a = AA();
            return "Test Index====>" + _http.RouteData + _http.GetQueryString();
        }

        public static int AA()
        {
            return 1000;
        }

        public static string Q(IFastHttp _http)
        {
            return _http.GetQueryString();
        }

        [FastPost]
        public static JsonObject F(IFastHttp _http)
        {
            return _http.GetQueryAndForm();
        }

        [FastRateLimit(1)]
        public static async Task<string> R1(int a, IFastHttp _http)
        {
            if (a == 0)
            {
                await Task.Delay(500000, _http.Token);
            }

            return "R1" + a + _http.GetQuery("b");
        }

        public static string R2()
        {
            return "R2";
        }

        public static string R3(IFastHttp _http)
        {
            _http.SetFastException("R3异常" + _http.RouteData);
            return "R3";
        }

        public static string RwR4(IFastHttp _http)
        {
            throw new FastException("R4异常");
            return "R4";
        }

        [FastPost]
        public static async Task<string> Upload(IFastHttp http)
        {
            return "返回空置";
        }

        [FastPost]
        public static async Task<string> Uploads(IFastHttp http)
        {
            return "返回没有";
        }

        [FastPost]
        public static JsonObject Json(JsonObject jobj, People p, string j)
        {
            Console.WriteLine(jobj["name"].GetValue<string>());
            return jobj;
        }

        public static People Bind(IFastHttp http)
        {
            return http.QueryBind<People>();
        }

        public static People Bind2(IFastHttp http)
        {
            return http.FormBind<People>();
        }

        public static FastView View(People p)
        {
            if (string.IsNullOrEmpty(p.Name))
            {
                p.Name = "李四";
            }

            return new FastView
            {
                ViewPath = "Views/test.html",
                ViewData = p
            };
        }

        public static string Get()
        {
            return "123";
        }

        public People GetPeople()
        {
            return new People
            {
                Name = "李四",
                AddTime = DateTime.Now,
                AddTime2 = DateTime.Now,
                EditTime = DateTime.Now,
                EditTime2 = null
            };
        }

        public People GetPeople2(IFastHttp http)
        {
            var jsonString = """
                {"Id":0,"Name":"李四","AddTime":"2023-05-04 09:22:14","AddTime2":null,"EditTime":"2023-05-04 09:22:14","EditTime2":null}
                """;
            var p = JsonSerializer.Deserialize<People>(jsonString, http.JsonOptions);
            return p;
        }

        [FastPost]
        public async Task Upload111(IFastHttp http)
        {
            var id = http.GetForm("id");
            var name = http.GetForm("naMe");
            Console.WriteLine(id + name);

            using var stream = http.GetFileStream(0);
            var len = http.GetFileLength("a");
            var fname = http.GetFileName(0);

            await http.WriteFileAsync(stream, fname, len);

        }

        [FastPost]
        public async Task ZZ(IFastHttp _http)
        {
            var json = await _http.GetJsonStringAsync();
            var json2 = await _http.GetJsonStringAsync();

            var model = await _http.GetJsonAsync<MySqlEntity>();
        }

        public async Task J(IFastHttp _http)
        {
            await _http.WriteJsonAsync(new { Name = "阿斯顿撒旦", Sex = 1 });
        }

        public async Task T(IFastHttp _http)
        {
            await _http.WriteTextAsync("十大大苏打实打实的1111");
        }

        public async Task D(IFastHttp _http)
        {
            await _http.WriteFileAsync(Encoding.UTF8.GetBytes("你好撒旦撒打算"), "1.txt");
        }

        public void Bd(IFastHttp _http)
        {
            _http.Redirect("https://www.baidu.com");
        }

        public async IAsyncEnumerable<int> I()
        {
            for (int i = 0; i < 5; i++)
            {
                await Task.Delay(1000);
                if (i == 2)
                {
                    throw new FastException("no");
                }
                yield return i;
            }
        }

        public async Task Dbb(IFastHttp _http)
        {
            var bf = DbHelper.Database.QueryUnbufferedAsync("select * from people");
            await _http.WriteJsonAsync(new { code = -1, data = bf, msg = "" });
        }

        public async IAsyncEnumerable<object> Dbb22(IFastHttp _http)
        {
            var bf = DbHelper.Database.QueryUnbufferedAsync("select * from people");

            await foreach (var item in bf)
            {
                await Task.Delay(1000);
                yield return item;
            }

            var bf2 = DbHelper.Database.QueryUnbufferedAsync("select * from a");

            await foreach (var item in bf2)
            {
                await Task.Delay(10);
                yield return item;
            }
        }

        [FastPost]
        [FastJsonStream]
        public async IAsyncEnumerable<int> TestIAs()
        {
            for (int i = 0; i < 10; i++)
            {
                await Task.Delay(1000);
                yield return i;
            }
        }

        [FastUpload]
        [FastDownload]
        public async Task UploadDown(IFastHttp _http)
        {
            var fileName = _http.GetFileName(0);
        }

        [FastPost]
        [FastPostStream]
        public async Task<string> PostStream([FastQuery] int id, IFastHttp _http)
        {
            using var reader = new StreamReader(_http.GetRequestStream());
            return await reader.ReadToEndAsync() + id;
        }

        [FastPost]
        [FastJsonStream]
        [FastPostStream]
        public async IAsyncEnumerable<string> PostStream2(IFastHttp _http)
        {
            using var reader = new StreamReader(_http.GetRequestStream());
            string line;

            while ((line = await reader.ReadLineAsync()) != null)
            {
                Console.WriteLine(line);
                yield return line;
            }
        }
    }
}
