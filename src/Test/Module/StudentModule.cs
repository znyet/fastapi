﻿using FastApi.Attributes;

namespace Test
{
    [FastRoute("student")]
    public class StudentModule
    {
        public string index()
        {
            return "student index";
        }

        public string Add(IFastHttp http)
        {
            return http.GetQuery("name") + http.UserData["aa"];
        }

        [FastRedirect]
        public string baidu()
        {
            return "https://www.baidu.com";
        }

        public async Task Down(IFastHttp http)
        {
            await http.WriteFileAsync(new byte[] { 1, 23, 4, 4 }, "下载的文件.txt");
        }

        public void Jd(IFastHttp http)
        {
            http.Redirect("https://www.jd.com");
        }

    }
}
