﻿using Autofac;
using FastApi.Attributes;

namespace Test
{
    [FastRoute("test2")]
    public class Test2 : TestBase
    {
        public Test2(ILifetimeScope scope, IFastHttp http) : base(scope, http)
        {

        }

        public string Db()
        {
            var db = scope.Resolve<IMyDatabase>();
            return db.Get();
        }

        public string GetName()
        {
            return http.GetQuery("name");
        }

        [FastPost]
        [FastUpload]
        public void GetUpload()
        {

        }

        [FastPost]
        [FastUpload]
        public string GetUpload2([FastForm] string name, [FastForm] DateTime time)
        {
            return name + time;
        }

        [FastPost]
        [FastUpload]
        [FastDownload]
        public async Task GetUpload3([FastForm] string name, IFastHttp http)
        {
            var file = http.GetFileLength(0);
            await http.WriteFileAsync(new byte[] { 1, 23, 4, 4 }, "下载的文件.txt");
        }

        public string Getdddd(int id, string name, decimal money, DateTime time)
        {
            return id + name + money + time.ToString();
        }

        [FastDownload]
        public void GetDownload()
        {
            
        }

        [FastPost]
        [FastDownload]
        public void PostDownload()
        {

        }

    }

    public class TestBase
    {
        internal protected ILifetimeScope scope;
        internal protected IFastHttp http;

        public TestBase(ILifetimeScope scope, IFastHttp http)
        {
            this.scope = scope;
            this.http = http;
        }
    }
}
