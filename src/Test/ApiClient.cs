﻿using Flurl.Http;
using OkFlurl;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace Test
{
    public partial class ApiClient
    {
        public ApiClient(string url)
        {
            Api = new FlurlApi(url);
        }

        #region common

        public FlurlApi Api { get; }

        public static T GetJsonData<T>(JsonElement el, IFlurlRequest req)
        {
            var code = el.GetProperty("code").GetInt32();
            if (code < 0)
            {
                var msg = el.GetProperty("msg").GetString();
                throw new Exception($"{req?.Url}==>{msg}");
            }
            var data = el.GetProperty("data");
            if (data.ValueKind == JsonValueKind.Null)
            {
                return default;
            }
            return data.Deserialize<T>(FlurlExt.JsonOptions);
        }

        #endregion

        #region api


        public async Task<Test.Dto.MySqlEntity> Home_A(Test.Dto.MySqlEntity entity, string json, System.Text.Json.Nodes.JsonObject obj, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<Test.Dto.MySqlEntity>(_response, _request);

        }


        public async Task<System.Collections.Generic.List<Test.Dto.MySqlEntity>> Home_A2(Test.Dto.MySqlEntity entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a2");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<System.Collections.Generic.List<Test.Dto.MySqlEntity>>(_response, _request);

        }


        public async Task<Test.Dto.MySqlEntity[]> Home_A3(Test.Dto.MySqlEntity entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a3");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<Test.Dto.MySqlEntity[]>(_response, _request);

        }


        public async Task<System.Collections.Generic.List<Test.Dto.MySqlEntity>> Home_A4(Test.Dto.MySqlEntity entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a4");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<System.Collections.Generic.List<Test.Dto.MySqlEntity>>(_response, _request);

        }


        public async Task<System.Collections.Generic.List<Test.Dto.MySqlEntity>> Home_A5(Test.Dto.MySqlEntity[] entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a5");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<System.Collections.Generic.List<Test.Dto.MySqlEntity>>(_response, _request);

        }


        public async Task<System.Collections.Generic.List<Test.Dto.MySqlEntity>> Home_A6(System.Collections.Generic.List<Test.Dto.MySqlEntity> entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a6");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<System.Collections.Generic.List<Test.Dto.MySqlEntity>>(_response, _request);

        }


        public async Task<Test.Dto.BaseEntity<System.Collections.Generic.List<Test.Dto.MySqlEntity>>> Home_A7(Test.Dto.BaseEntity<Test.Dto.MySqlEntity> entity, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/a7");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, entity, cancellationToken);
            return GetJsonData<Test.Dto.BaseEntity<System.Collections.Generic.List<Test.Dto.MySqlEntity>>>(_response, _request);

        }


        public async Task<string> Home_Add(string a, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/add");
            _request.SetQueryParam("a", a);
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Home_Add2(string a, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/add2");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, a, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Home_UploadFiles(int id, string name, string sex, int age, string address, string addrss, string phone, FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/uploadFiles");
            _request.SetQueryParam("id", id);
            _request.SetQueryParam("name", name);
            _request.SetQueryParam("sex", sex);
            _request.SetQueryParam("age", age);
            flurlForm.AddString("address", address);
            flurlForm.AddString("addrss", addrss);
            flurlForm.AddString("phone", phone);
            var _response = await Api.FormGetJsonAsync<JsonElement>(_request, null, flurlForm, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Home_TestAsync(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/test");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Home_VAsync(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/v");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public Task<Stream> Home_MyAsync(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/my");
            return Api.GetStreamAsync(_request, null, cancellationToken);

        }


        public async Task Home_db(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/db");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Home_db2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/db2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Home_db3(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/db3");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<long> Home_dbQuery(System.Collections.Generic.List<System.Int32> aa, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/dbQuery");
            Api.InitGetPar(_request, aa);
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<long>(_response, _request);

        }


        public async Task<string> Home_Autofac(int id, string name, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/autofac");
            _request.SetQueryParam("id", id);
            _request.SetQueryParam("name", name);
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Home_Http(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/http");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task Home_AddP(Test.People p, FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/addP");
            Api.InitFormPar(flurlForm, p);
            var _response = await Api.FormGetJsonAsync<JsonElement>(_request, null, flurlForm, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<Test.People> Home_All(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/all");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Test.People>(_response, _request);

        }


        public async Task<Dapper.Sharding.PageEntity<Test.People>> Home_page(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/page");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Dapper.Sharding.PageEntity<Test.People>>(_response, _request);

        }


        public async Task Home_Down(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/down");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<System.Int32>> Home_TestIAsync1(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/testIAsync1");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<System.Int32>>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<Test.People>> Home_TestIAsync2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/testIAsync2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<Test.People>>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<Test.People>> Home_TestIAsyncDb(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/testIAsyncDb");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<Test.People>>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<Test.People>> Home_TestIAsyncDb2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/testIAsyncDb2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<Test.People>>(_response, _request);

        }


        public async Task<string> Home_TestGet(int id, string name, double dv, decimal dc, DateTime now, bool ok, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/testGet");
            _request.SetQueryParam("id", id);
            _request.SetQueryParam("name", name);
            _request.SetQueryParam("dv", dv);
            _request.SetQueryParam("dc", dc);
            _request.SetQueryParam("now", now.ToString(Api.TimeFormat));
            _request.SetQueryParam("ok", ok);
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task Home_ZZZ(Test.People p, Test.People p2, Test.People p3, Test.People p4, FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/home/zZZ");
            Api.InitFormPar(flurlForm, p);
            Api.InitFormPar(flurlForm, p2);
            Api.InitGetPar(_request, p3);
            Api.InitGetPar(_request, p4);
            var _response = await Api.FormGetJsonAsync<JsonElement>(_request, null, flurlForm, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<string> Test_Index(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/index");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<int> Test_AA(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/aA");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<int>(_response, _request);

        }


        public async Task<string> Test_Q(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/q");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<System.Text.Json.Nodes.JsonObject> Test_F(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/f");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Text.Json.Nodes.JsonObject>(_response, _request);

        }


        public async Task<string> Test_R1(int a, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/r1");
            _request.SetQueryParam("a", a);
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test_R2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/r2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test_R3(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/r3");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test_RwR4(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/rwR4");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test_Upload(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/upload");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test_Uploads(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/uploads");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<System.Text.Json.Nodes.JsonObject> Test_Json(System.Text.Json.Nodes.JsonObject jobj, Test.People p, string j, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/json");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, jobj, cancellationToken);
            return GetJsonData<System.Text.Json.Nodes.JsonObject>(_response, _request);

        }


        public async Task<Test.People> Test_Bind(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/bind");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Test.People>(_response, _request);

        }


        public async Task<Test.People> Test_Bind2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/bind2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Test.People>(_response, _request);

        }


        public async Task<string> Test_Get(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/get");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<Test.People> Test_GetPeople(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/getPeople");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Test.People>(_response, _request);

        }


        public async Task<Test.People> Test_GetPeople2(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/getPeople2");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<Test.People>(_response, _request);

        }


        public async Task Test_Upload111(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/upload111");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Test_ZZ(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/zZ");
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Test_J(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/j");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Test_T(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/t");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Test_D(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/d");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Test_Bd(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/bd");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<System.Int32>> Test_I(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/i");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<System.Int32>>(_response, _request);

        }


        public async Task Test_Dbb(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/dbb");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<System.Collections.Generic.IAsyncEnumerable<System.Object>> Test_Dbb22(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/dbb22");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<System.Collections.Generic.IAsyncEnumerable<System.Object>>(_response, _request);

        }


        public System.Collections.Generic.IAsyncEnumerable<System.Int32> Test_TestIAs(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/testIAs");
            return Api.PostGetJsonStreamAsync<System.Int32>(_request, null, cancellationToken);

        }


        public Task<(Stream stream, string name, long? length)> Test_UploadDown(FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/uploadDown");
            return Api.GetDownloadStreamAsync(_request, null, cancellationToken);

        }


        public async Task<string> Test_PostStream(int id, FlurlStreamContent streamContent, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/postStream");
            _request.SetQueryParam("id", id);
            var _response = await Api.PostGetJsonAsync<JsonElement>(_request, streamContent, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public System.Collections.Generic.IAsyncEnumerable<System.String> Test_PostStream2(FlurlStreamContent streamContent, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test/postStream2");
            return Api.PostGetJsonStreamAsync<System.String>(_request, streamContent, cancellationToken);

        }


        public async Task<string> Student_index(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/student/index");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Student_Add(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/student/add");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task Student_Down(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/student/down");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task Student_Jd(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/student/jd");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<string> Test2_Db(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/db");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task<string> Test2_GetName(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getName");
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public async Task Test2_GetUpload(FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getUpload");
            var _response = await Api.FormGetJsonAsync<JsonElement>(_request, null, flurlForm, cancellationToken);
            GetJsonData<JsonNode>(_response, _request);

        }


        public async Task<string> Test2_GetUpload2(string name, DateTime time, FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getUpload2");
            flurlForm.AddString("name", name);
            flurlForm.AddString("time", time.ToString(Api.TimeFormat));
            var _response = await Api.FormGetJsonAsync<JsonElement>(_request, null, flurlForm, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public Task<(Stream stream, string name, long? length)> Test2_GetUpload3(string name, FlurlForm flurlForm, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getUpload3");
            flurlForm.AddString("name", name);
            return Api.FormDownloadStreamAsync(_request, null, flurlForm, cancellationToken);

        }


        public async Task<string> Test2_Getdddd(int id, string name, decimal money, DateTime time, CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getdddd");
            _request.SetQueryParam("id", id);
            _request.SetQueryParam("name", name);
            _request.SetQueryParam("money", money);
            _request.SetQueryParam("time", time.ToString(Api.TimeFormat));
            var _response = await Api.GetJsonAsync<JsonElement>(_request, null, cancellationToken);
            return GetJsonData<string>(_response, _request);

        }


        public Task<(Stream stream, string name, long? length)> Test2_GetDownload(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/getDownload");
            return Api.GetDownloadStreamAsync(_request, null, cancellationToken);

        }


        public Task<(Stream stream, string name, long? length)> Test2_PostDownload(CancellationToken cancellationToken = default)
        {
            var _request = Api.CreateRequest("/test2/postDownload");
            return Api.PostDownloadStreamAsync(_request, null, cancellationToken);

        }

        #endregion
    }
}
