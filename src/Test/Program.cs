using Autofac;
using Dapper.Sharding;
using FastApi;
using FastApi.Attributes;
using JinianNet.JNTemplate;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Text.Json;

namespace Test
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.WebHost.UseKestrel(opt =>
            {
                opt.Limits.MaxRequestBodySize = null; //  不限制上传文件大小
                //opt.Limits.MaxRequestLineSize = int.MaxValue; //请求行的最大允许大小 默认8k
                //opt.Limits.MaxRequestBufferSize = int.MaxValue; //请求缓冲区 默认1M

                opt.Limits.MinRequestBodyDataRate =
                    new MinDataRate(bytesPerSecond: 80, gracePeriod: TimeSpan.FromSeconds(60));
                opt.Limits.MinResponseDataRate =
                    new MinDataRate(bytesPerSecond: 80, gracePeriod: TimeSpan.FromSeconds(60));

            });

            builder.Services.Configure<FormOptions>(opt =>
            {
                opt.ValueLengthLimit = int.MaxValue;
                opt.MultipartBodyLengthLimit = int.MaxValue; //上传文件大小默认 128M
            });

            #region Cors

            string corsName = "cors";

            builder.Services.AddCors(corsOpt =>
            {
                corsOpt.AddPolicy(corsName, policy =>
                {
                    policy.AllowAnyOrigin();
                    policy.AllowAnyMethod();
                    policy.AllowAnyHeader();
                });
            });

            #endregion

            // Add services to the container.
            builder.Services.AddControllersWithViews();

            var app = builder.Build();

            //使用跨域
            app.UseCors(corsName);

            app.MapGet("/", context =>
            {
                context.Response.Redirect("login.html");
                return Task.CompletedTask;
            });

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            //创建FastApp
            var fastApp = new FastApp(app);

            #region 添加自定义参数

            fastApp.AddParams(typeof(LoginUser), new FastParamCreator
            {
                Create = ctx =>
                {
                    return ctx.UserData["user"];
                }
            });

            fastApp.AddParams(typeof(ILifetimeScope), new FastParamCreator
            {
                Create = ctx =>
                {
                    return AutofacHelper.GetScope();
                },
                DisposeValueAsync = (ctx, obj) =>
                {
                    var scope = (ILifetimeScope)obj;
                    return scope.DisposeAsync();
                }
            });

            fastApp.AddParams(typeof(DistributedTransaction), new FastParamCreator
            {
                Create = ctx =>
                {
                    var tran = new DistributedTransaction();
                    return tran;
                },
                //DisposeAsync = async (ctx, obj) =>
                //{
                //    var tran = (DistributedTransaction)obj;
                //    await tran.DisposeAsync();
                //},
                CommitAsync = (ctx, obj) =>
                {
                    var tran = (DistributedTransaction)obj;
                    return tran.CommitAsync();
                },
                RollbackAsync = (ctx, obj) =>
                {
                    var tran = (DistributedTransaction)obj;
                    return tran.RollbackAsync();
                }
            });

            #endregion

            //过滤器
            fastApp.AddFilter(new FastFilter
            {
                ExecuteAsync = async fastCtx =>
                {
                    string token = fastCtx.HttpContext.Request.Headers["token"];
                    if (string.IsNullOrEmpty(token))
                    {
                        fastCtx.Ex = new FastException("授权失败");
                        return;
                    }

                    fastCtx.UserData.Add("user", new LoginUser() { Name = "李四" });
                    fastCtx.UserData.Add("aa", "用户自定义数据");
                }
            });

            //添加路由
            fastApp.AddRoute("home", typeof(HomeModule));
            fastApp.AddRoute("test", typeof(TestModule));
            fastApp.AddRoute(typeof(StudentModule).Assembly);

            //限流器
            var rateLimit = new FastRateLimit();

            //开始请求前
            fastApp.OnRequest += (fastCtx) =>
            {
                //【全局限流器】最多10个并发
                var ok = rateLimit.WaitOne(fastCtx, "key", 10);

                //【FastRateLimitAttribute】特性限流器
                if (ok && fastCtx.Action.RateLimitCount > 0)
                {
                    rateLimit.WaitOne(fastCtx, fastCtx.Action.RateLimitKey, fastCtx.Action.RateLimitCount);
                }

                Console.WriteLine($"OnRequest===>{fastCtx.RouteData}");
            };

            //响应数据前（统一返回json格式）
            fastApp.OnResponseAsync += (fastCtx) =>
            {
                var result = new { code = 0, data = fastCtx.ResponseData, msg = "", time = fastCtx.ExecuteTime };
                return fastCtx.HttpContext.Response.WriteAsJsonAsync(result, fastApp.JsonOptions, fastCtx.Token);
            };

            //请求结束后
            fastApp.OnAfterRequestAsync += async (fastCtx) =>
            {
                //【全局限流器】释放
                var ok = rateLimit.Release(fastCtx, "key");

                //【FastRateLimitAttribute】特性限流器释放
                if (ok && fastCtx.Action.RateLimitCount > 0)
                {
                    rateLimit.Release(fastCtx, fastCtx.Action.RateLimitKey);
                }

                Console.WriteLine($"OnAfterRequestAsync，{fastCtx.Route}==>方法执行时间：{fastCtx.ExecuteTime}ms，序列化执行时间：{fastCtx.SerialTime}ms");
                Console.WriteLine($"QueryForm请求方法参数：{JsonSerializer.Serialize(fastCtx.FastHttp?.GetQueryAndForm(), fastCtx.JsonOptions)}");
                Console.WriteLine($"HttpRequest请求参数：{JsonSerializer.Serialize(fastCtx.GetRequestData(), fastCtx.JsonOptions)}");


                if (fastCtx.Ex != null) //发生异常
                {
                    //如果可以响应,则响应错误信息
                    if (fastCtx.CanResponse)
                    {
                        try
                        {
                            var result = new { code = -1, data = null as string, msg = fastCtx.Ex.Message, time = fastCtx.ExecuteTime };
                            await fastCtx.HttpContext.Response.WriteAsJsonAsync(result, fastApp.JsonOptions);
                        }
                        catch { }
                    }
                    Console.WriteLine($"OnAfterRequestAsync，{fastCtx.Route}==>发生异常:{fastCtx.Ex.Message}" + fastCtx.Ex.StackTrace);
                }
            };

            //响应视图
            fastApp.OnViewAsync += async (fastCtx, view) =>
            {
                var template = Engine.LoadTemplate(AppDomain.CurrentDomain.BaseDirectory + view.ViewPath);
                template.Set("Model", view.ViewData, view.ViewData.GetType());
                var html = await template.RenderAsync();
                await fastCtx.HttpContext.Response.WriteAsync(html);
            };

            Console.WriteLine("IsDevelopment==>" + fastApp.IsDevelopment);
            Console.WriteLine("IsProduction==>" + fastApp.IsProduction);
            Console.WriteLine(fastApp.BaseDirectory);
            Console.WriteLine(fastApp.ContentRootPath);
            Console.WriteLine(fastApp.WebRootPath);

#if DEBUG

            #region Swagger

            var fastSwag = new FastApi.Swag.FastSwag(fastApp.FastModuleDict);
            fastSwag.AddApiKey("token");
            //fastSwag.AddJwtBearer();
            fastSwag.OnCreateDocument += (doc) =>
            {
                doc.Info.Title = "API接口信息";
                doc.Info.Description = "这是对接口信息的描述11";
            };
            fastSwag.Version = FastApi.Swag.FastSwagVersion.V50;

            #region Obsolete

            //fastSwag.OnResponse += (baseSchema) =>
            //{
            //    var schema = new NJsonSchema.JsonSchema();
            //    schema.Type = NJsonSchema.JsonObjectType.Object;
            //    schema.Properties.Add("code", new NJsonSchema.JsonSchemaProperty { Type = NJsonSchema.JsonObjectType.Integer });
            //    schema.Properties.Add("msg", new NJsonSchema.JsonSchemaProperty { Type = NJsonSchema.JsonObjectType.String });
            //    var dataPro = new NJsonSchema.JsonSchemaProperty();
            //    if (baseSchema.Example != null)
            //    {
            //        dataPro.ActualSchema.Example = baseSchema.Example;
            //    }
            //    else
            //    {
            //        //dataPro.AdditionalPropertiesSchema = baseSchema.ActualSchema;
            //    }
            //    schema.Properties.Add("data", dataPro);
            //    return schema;
            //};

            #endregion

            fastSwag.CreateApiJsonFile($@"{Environment.CurrentDirectory}\wwwroot");

            Console.WriteLine("Swagger文档生成成功!");

            #endregion

            #region ClientSdk Project(创建SDK给第三方调用)

            var sdk = new FastClientSdk("MyApiSdk");
            sdk.OnMessage += Console.WriteLine;

            //add nuget package
            sdk.Packages.Add("Dapper.Sharding");

            //fix namespace
            sdk.UsingPrefix.Add("Test.Dto");

            //ignore folder
            sdk.AddIgnoreFolder(@"..\Test.Dto\bin");
            sdk.AddIgnoreFolder(@"..\Test.Dto\obj");

            //add class folder
            sdk.AddClassFolder(@"..\Test.Dto");

            //add class file
            sdk.AddClassFile(@".\People.cs");

            //create client sdk project
            await sdk.CreateProjectAsync(@"C:\", fastApp.FastModuleDict);

            //build project release dll.
            await sdk.BuildProjectAsync();

            #endregion

            #region ClientSdk File

            var sdk2 = new FastClientSdk("Test");
            await sdk2.CreateSdkFileAsync(@".\", fastApp.FastModuleDict);

            #endregion

#endif

            //静态资源浏览
            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true
            });

            app.Run();
        }
    }

    class LoginUser
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}