﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// 上传
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastUploadAttribute : Attribute
    {

    }
}
