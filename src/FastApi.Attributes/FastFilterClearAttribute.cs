﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// 清除过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastFilterClearAttribute : Attribute
    {

    }
}
