﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// Get参数
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class FastQueryAttribute : Attribute
    {

    }
}
