﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// Form参数
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class FastFormAttribute : Attribute
    {

    }
}
