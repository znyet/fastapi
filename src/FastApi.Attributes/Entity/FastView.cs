﻿namespace FastApi.Attributes
{
    /// <summary>
    /// 视图
    /// </summary>
    public class FastView
    {
        public string ViewPath { get; set; }

        public object ViewData { get; set; }
    }
}
