﻿using System;

namespace FastApi.Attributes
{
    public class FastException : Exception
    {
        public string Message { get; set; }

        public int Code { get; set; }

        public new object Data { get; set; }

        public FastException()
        {
            
        }

        public FastException(string message, int code = -1, object data = null) : base(message)
        {
            Message = message;
            Code = code;
            Data = data;
        }
    }
}
