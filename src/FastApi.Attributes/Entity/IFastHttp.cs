﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Nodes;


namespace FastApi.Attributes
{
    public interface IFastHttp
    {
        #region Com

        void Redirect(string url);

        Dictionary<string, object> UserData { get; }

        void SetException(Exception ex);

        void SetFastException(string msg, int code = -1, object data = null);

        string Route { get; }

        string RouteData { get; }

        JsonSerializerOptions JsonOptions { get; }

        void SetJsonOptions(JsonSerializerOptions options);

        void SetCustomer(bool ok);

        void SetStatusCode(int code);

        #endregion

        #region Header

        long? GetReqContentLength();

        ICollection<string> GetReqHeaderKeys();

        string GetReqHeader(string key);

        void SetResHeader(string key, string value);

        void RemoveResHeader(string key);

        void SetResContentType(string val);

        void SetResContentLength(long? length);

        void SetResFileName(string name);

        #endregion

        #region Query

        ICollection<string> GetQueryKeys();

        string GetQuery(string key);

        T GetQuery<T>(string key);

        T QueryBind<T>();

        void QueryBindTo(object model);

        string GetQueryString();

        #endregion

        #region Form

        ICollection<string> GetFormKeys();

        string GetForm(string key);

        T GetForm<T>(string key);

        T FormBind<T>();

        void FormBindTo(object model);

        #endregion

        JsonObject GetQueryAndForm();

        #region Cookie

        ICollection<string> GetCookieKeys();

        string GetCookie(string key);

        void SetCookie(string key, string value);

        void SetCookie(string key, string value, DateTimeOffset expires);

        void DeleteCookie(string key);

        #endregion

        #region ConnectionInfo

        string GetIpv4();

        string GetIpv4ByHeader(string header = "X-Forwarded-For");

        string GetIpv6();

        (string scheme, string host, string path, string pathBase, string queryString, bool isHttps) GetReqInfo();

        CancellationToken Token { get; }

        #endregion

        #region GetFile

        IEnumerable<string> GetFileNameList();

        string GetFileName(string name);

        int GetFileCount();

        string GetFileName(int index);

        long GetFileLength(string name);

        long GetFileLength(int index);

        Stream GetFileStream(string name);

        Stream GetFileStream(int index);

        (Stream stream, string fileName, long length) GetFileStreamInfo(string name);

        (Stream stream, string fileName, long length) GetFileStreamInfo(int index);

        #endregion

        #region WriteFile

        Task WriteFileAsync(ReadOnlyMemory<byte> buffer, string fileName = null);

        Task WriteFileAsync(Stream stream, string fileName = null, long? length = null, int bufferSize = 4096);

        Task WriteFileAsync(string filePath, string fileName = null, int bufferSize = 4096);

        #endregion

        #region SaveFile

        Task<string> SaveFileAsync(string name, string folder, string saveName = null);

        Task<string> SaveFileAsync(int index, string folder, string saveName = null);

        Task SaveFileAsync(string name, Stream stream);

        Task SaveFileAsync(int index, Stream stream);

        #endregion

        #region Body

        Stream GetRequestStream();

        Stream GetResponseStream();

        Task<string> GetJsonStringAsync();

        ValueTask<T> GetJsonAsync<T>(JsonSerializerOptions options = null);

        ValueTask<object> GetJsonAsync(Type type, JsonSerializerOptions options = null);

        IAsyncEnumerable<T> GetJsonStreamAsync<T>();

        #endregion

        ValueTask WriteBytesAsync(ReadOnlyMemory<byte> buffer);

        Task WriteBytesFlushAsync(ReadOnlyMemory<byte> buffer);

        Task WriteJsonAsync<TValue>(TValue value, JsonSerializerOptions options = null);

        Task WriteTextAsync(string text);

        Task WriteTextAsync(string text, Encoding encoding);

        Task FlushAsync();

        ValueTask DisposeAsync();
    }
}
