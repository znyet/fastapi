﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// 下载
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastDownloadAttribute : Attribute
    {

    }
}
