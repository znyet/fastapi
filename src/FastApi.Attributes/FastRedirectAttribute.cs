﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// Url跳转
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastRedirectAttribute : Attribute
    {

    }
}
