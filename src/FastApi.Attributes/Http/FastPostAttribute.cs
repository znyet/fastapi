﻿using System;

namespace FastApi.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastPostAttribute : Attribute
    {
        public FastPostAttribute(bool enableBuffering = false) 
        {
            EnableBuffering = enableBuffering;
        }

        public bool EnableBuffering { get; }

    }
}
