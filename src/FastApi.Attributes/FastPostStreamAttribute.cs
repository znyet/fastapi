﻿using System;

namespace FastApi.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastPostStreamAttribute : Attribute
    {
    }
}
