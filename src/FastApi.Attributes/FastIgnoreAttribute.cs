﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// 忽略action
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastIgnoreAttribute : Attribute
    {

    }
}
