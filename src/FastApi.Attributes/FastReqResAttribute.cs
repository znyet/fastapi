﻿using System;

namespace FastApi.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastReqResAttribute : Attribute
    {
        public FastReqResAttribute(Type reqType, Type resType)
        {
            ReqType = reqType;
            ResType = resType;
        }

        public Type ReqType { get; }

        public Type ResType { get; }
    }
}
