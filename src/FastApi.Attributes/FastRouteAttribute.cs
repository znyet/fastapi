﻿using System;

namespace FastApi.Attributes
{
    /// <summary>
    /// 路由
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastRouteAttribute : Attribute
    {
        public FastRouteAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
