﻿using System;

namespace FastApi.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FastRateLimitAttribute : Attribute
    {
        public FastRateLimitAttribute(int count, string key = null, string errMsg = "FastRateLimit", int errCode = -1)
        {
            if (count <= 0)
            {
                throw new Exception("FastRateLimitAttribute=>count must greater then 0");
            }

            Count = count;
            Key = key;
            ErrMsg = errMsg;
            ErrCode = errCode;
        }

        public int Count { get; }

        public string Key { get; }

        public string ErrMsg { get; }

        public int ErrCode { get; }

    }
}
